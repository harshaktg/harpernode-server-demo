const express = require("express");
const { config } = require("dotenv");
const cors = require("cors");

config();

// var whitelist = [process.env.CLIENT_DOMAIN];
// var corsOptions = {
//   credentials: true,
//   origin: function (origin, callback) {
//     if (whitelist.indexOf(origin) !== -1) {
//       callback(null, true);
//     } else {
//       callback(new Error("Not allowed by CORS"));
//     }
//   },
// };

const corsOptions = {
  origin:
    process.env.CLIENT_DOMAIN || "https://react-vercel-test-rust.vercel.app",
  credentials: true,
};

const app = express();

app.use(cors(corsOptions));

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

const postRoutes = require("./routes/api/posts");
const tagRoutes = require("./routes/api/tags");
const authRoutes = require("./routes/api/auth");
const generalRoutes = require("./routes/api/general");
const bookmarkRoutes = require("./routes/api/bookmark");
const userRoutes = require("./routes/api/user");
const searchRoutes = require("./routes/api/search");

app.use("/auth", authRoutes);
app.use("/user", userRoutes);
app.use("/posts", postRoutes);
app.use("/tags", tagRoutes);
app.use("/bookmarks", bookmarkRoutes);
app.use("/search", searchRoutes);
app.use("/", generalRoutes);

app.get("/", (req, res) => {
  res.json({ text: "Hello HarperNode" });
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
