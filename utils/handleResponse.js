const handleResponse = (err, res, response) => {
  if (err) {
    response.status(500).json(err);
  } else {
    response.status(res.statusCode).json(res.data);
  }
};

module.exports = { handleResponse };
