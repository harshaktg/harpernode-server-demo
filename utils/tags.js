const TAGS = [
  {
    name: "tech-news",
  },
  {
    name: "general-programming",
  },
  {
    name: "security",
  },
  {
    name: "react",
  },
  {
    name: "webdev",
  },
  {
    name: "javascript",
  },
  {
    name: "data-science",
  },
  {
    name: "google",
  },
  {
    name: "cloud",
  },
  {
    name: "business",
  },
  {
    name: "startup",
  },
  {
    name: "aws",
  },
  {
    name: "machine-learning",
  },
  {
    name: "apple",
  },
  {
    name: "career",
  },
  {
    name: "microsoft",
  },
  {
    name: "ai",
  },
  {
    name: "cyber",
  },
  {
    name: "python",
  },
  {
    name: "testing",
  },
  {
    name: "nodejs",
  },
  {
    name: "azure",
  },
  {
    name: "infrastructure",
  },
  {
    name: "css",
  },
  {
    name: "hardware",
  },
  {
    name: "devtools",
  },
  {
    name: "kubernetes",
  },
  {
    name: "java",
  },
  {
    name: "deep-learning",
  },
  {
    name: "linux",
  },
  {
    name: "architecture",
  },
  {
    name: "venture-capital",
  },
  {
    name: "open-source",
  },
  {
    name: "github",
  },
  {
    name: "c",
  },
  {
    name: "devops",
  },
  {
    name: "sap",
  },
  {
    name: "logging",
  },
  {
    name: "html",
  },
  {
    name: "r",
  },
  {
    name: "containers",
  },
  {
    name: "ios",
  },
  {
    name: "data-privacy",
  },
  {
    name: "backend",
  },
  {
    name: "sql",
  },
  {
    name: "vuejs",
  },
  {
    name: "authentication",
  },
  {
    name: "docker",
  },
  {
    name: "angular",
  },
  {
    name: "typescript",
  },
  {
    name: "wordpress",
  },
  {
    name: "spring",
  },
  {
    name: "golang",
  },
  {
    name: "iot",
  },
  {
    name: "web-design",
  },
  {
    name: "google-chrome",
  },
  {
    name: "automation",
  },
  {
    name: "flutter",
  },
  {
    name: "crypto",
  },
  {
    name: "git",
  },
  {
    name: "gcp",
  },
  {
    name: "ruby",
  },
  {
    name: "react-native",
  },
  {
    name: "game-development",
  },
  {
    name: "graphql",
  },
  {
    name: "nlp",
  },
  {
    name: "serverless",
  },
  {
    name: "gaming",
  },
  {
    name: "rails",
  },
  {
    name: "php",
  },
  {
    name: "mongodb",
  },
  {
    name: "agile",
  },
  {
    name: "cicd",
  },
  {
    name: "big-data",
  },
  {
    name: "swift",
  },
  {
    name: "rust",
  },
  {
    name: "bots",
  },
  {
    name: "apache",
  },
  {
    name: "aspnet",
  },
  {
    name: "blockchain",
  },
  {
    name: "ecommerce",
  },
  {
    name: "postgresql",
  },
  {
    name: "tensorflow",
  },
  {
    name: "laravel",
  },
  {
    name: "firebase",
  },
  {
    name: ".net",
  },
  {
    name: "vr",
  },
  {
    name: "math",
  },
  {
    name: "kafka",
  },
  {
    name: "terraform",
  },
  {
    name: "apache-spark",
  },
  {
    name: "nextjs",
  },
  {
    name: "ar",
  },
  {
    name: "cryptography",
  },
  {
    name: "elk",
  },
  {
    name: "ibm",
  },
  {
    name: "webpack",
  },
  {
    name: "mysql",
  },
  {
    name: "kotlin",
  },
  {
    name: "microsoft-sql-server",
  },
  {
    name: "adobe",
  },
  {
    name: "oracle",
  },
  {
    name: "crawling",
  },
  {
    name: "game-design",
  },
  {
    name: "red-hat",
  },
  {
    name: "accessibility",
  },
  {
    name: "nocode",
  },
  {
    name: "bi",
  },
  {
    name: "bootstrap-css",
  },
  {
    name: "android-development",
  },
  {
    name: "computer-vision",
  },
  {
    name: "firefox",
  },
  {
    name: "xamarin",
  },
  {
    name: "gitlab",
  },
  {
    name: "tailwind-css",
  },
  {
    name: "redis",
  },
  {
    name: "blazor",
  },
  {
    name: "robotics",
  },
  {
    name: "cms",
  },
  {
    name: "data-analysis",
  },
  {
    name: "pytorch",
  },
  {
    name: "openshift",
  },
  {
    name: "seo",
  },
  {
    name: "heroku",
  },
  {
    name: "django",
  },
  {
    name: "gatsby",
  },
  {
    name: "scala",
  },
  {
    name: "bluetooth",
  },
  {
    name: "webassembly",
  },
  {
    name: "jupyter",
  },
  {
    name: "ethics",
  },
  {
    name: "elixir",
  },
  {
    name: "bash",
  },
  {
    name: "symfony",
  },
  {
    name: "jquery",
  },
  {
    name: "salesforce",
  },
  {
    name: "safari",
  },
  {
    name: "powershell",
  },
  {
    name: "erp",
  },
  {
    name: "netlify",
  },
  {
    name: "selenium",
  },
  {
    name: "nginx",
  },
  {
    name: "cloudflare",
  },
  {
    name: "ibm-cloud",
  },
  {
    name: "digitalocean",
  },
  {
    name: "mozilla",
  },
  {
    name: "flask",
  },
  {
    name: "embedded",
  },
  {
    name: "jenkins",
  },
  {
    name: "auth0",
  },
  {
    name: "keras",
  },
  {
    name: "ionic",
  },
  {
    name: "svelte",
  },
  {
    name: "lightbend",
  },
  {
    name: "haskell",
  },
  {
    name: "chromium",
  },
  {
    name: "crm",
  },
  {
    name: "hashicorp",
  },
  {
    name: "grpc",
  },
  {
    name: "dart",
  },
  {
    name: "aiops",
  },
  {
    name: "julia",
  },
  {
    name: "microsoft-edge",
  },
  {
    name: "mulesoft",
  },
  {
    name: "perl",
  },
  {
    name: "deno",
  },
  {
    name: "grafana",
  },
  {
    name: "prometheus",
  },
  {
    name: "jamstack",
  },
  {
    name: "ansible",
  },
  {
    name: "vb",
  },
  {
    name: "nestjs",
  },
  {
    name: "okta",
  },
  {
    name: "scikit",
  },
  {
    name: "google-bigquery",
  },
  {
    name: "apache-cassandra",
  },
  {
    name: "active-directory",
  },
  {
    name: "firmware",
  },
  {
    name: "gradle",
  },
  {
    name: "emberjs",
  },
  {
    name: "chef",
  },
  {
    name: "confluent-cloud",
  },
  {
    name: "ravendb",
  },
  {
    name: "nativescript",
  },
  {
    name: "couchbase",
  },
  {
    name: "prisma",
  },
  {
    name: "alibaba-cloud",
  },
  {
    name: "neo4j",
  },
  {
    name: "rabbitmq",
  },
  {
    name: "faunadb",
  },
  {
    name: "pulumi",
  },
  {
    name: "apache-flink",
  },
  {
    name: "apache-airflow",
  },
  {
    name: "clojure",
  },
  {
    name: "influxdb",
  },
  {
    name: "vite",
  },
  {
    name: "lisp",
  },
  {
    name: "bitbucket",
  },
  {
    name: "fastify",
  },
  {
    name: "algolia",
  },
  {
    name: "react-query",
  },
  {
    name: "puppet",
  },
  {
    name: "ballerina",
  },
  {
    name: "fastapi",
  },
  {
    name: "elm",
  },
  {
    name: "preact",
  },
  {
    name: "mobile-design",
  },
  {
    name: "erlang",
  },
  {
    name: "supabase",
  },
  {
    name: "alpinejs",
  },
  {
    name: "winui",
  },
  {
    name: "harperdb",
  },
  {
    name: "lua",
  },
  {
    name: "dailydev",
  },
  {
    name: "web3",
  },
  {
    name: "gis",
  },
  {
    name: "sinatra",
  },
  {
    name: "apache-hadoop",
  },
  {
    name: "apache-activemq",
  },
  {
    name: "kerberos",
  },
  {
    name: "freedos",
  },
  {
    name: "godot",
  },
  {
    name: "adonisjs",
  },
  {
    name: "devrel",
  },
  {
    name: "hotwire",
  },
];

module.exports = { TAGS };
