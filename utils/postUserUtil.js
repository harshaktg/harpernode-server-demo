const db = require("../db/config");

const addCreatedUser = async (posts) => {
  const userIds = posts.map((post) => post.created_user);

  if (userIds.length === 0) {
    return [];
  }
  const userSearchOptions = {
    table: "user",
    attributes: ["*"],
    hashValues: userIds,
  };

  const associatedUsers = await db.searchByHash(userSearchOptions);
  const userData = associatedUsers.data;
  const finalPosts = posts.map((post) => ({
    ...post,
    created_user: userData.find(
      (user) => post.created_user === user.user_id
    ) || {
      name: "Deleted user",
      picture: null,
      user_id: null,
    },
  }));

  return finalPosts;
};

module.exports = { addCreatedUser };
