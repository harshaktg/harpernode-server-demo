const jwt = require("jsonwebtoken");
const db = require("../db/config");

const isAuthorised = (req, res, next) => {
  console.log(req.headers);
  console.log(req.headers.token);
  if (!req.headers.token) {
    return res.status(404).json({ error: "Unauthenticated access" });
  }
  jwt.verify(
    req.headers.token,
    process.env.JWT_GOOGLE_SECRET,
    async function (err, decodedToken) {
      if (err) {
        console.log(err);
        res.status(500).json({ error: "Internal server error" });
      }
      if (decodedToken) {
        const options = {
          table: "user",
          hashValues: [decodedToken.user_id],
          attributes: ["*"],
        };
        console.log("options are ", options);
        try {
          const user = await db.searchByHash(options);
          const followersQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE user_id='${decodedToken.user_id}'`;
          const followingQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE follower_id='${decodedToken.user_id}'`;
          const allFollowers = await db.query(followersQuery);
          const allFollowing = await db.query(followingQuery);

          const followersIds = allFollowers.data.map(
            (follower) => follower.follower_id
          );
          const followingIds = allFollowing.data.map(
            (follower) => follower.user_id
          );

          if (user.statusCode === 200) {
            if (user.data.length) {
              req.user = {
                ...user.data[0],
                followers: followersIds,
                following: followingIds,
              };
              next();
            } else {
              return res.status(404).json({ error: "Unauthorized access" });
            }
          }
        } catch (err) {
          return res.status(500).json({ error: "Internal Server Error" });
        }
      }
    }
  );
};

module.exports = { isAuthorised };
