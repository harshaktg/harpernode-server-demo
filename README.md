# HarperNode

HarperNode is a lite version of Hashnode built using Express/Node.js along with HarperDB.


> This repository contains the server side code

## Live Demo

Click [here](https://harpernode-client.vercel.app/) to view the complete app 

---

## Run locally

To run the code locally, create an `.env` file in the root directory to configure environment variables. The file should contain these props:


```
PORT=<Port to run your server>
INSTANCE_URL=<HarperDB cloud instance>
INSTANCE_USERNAME=<HarperDB instance username>
INSTANCE_PASSWORD=<HarperDB instance password>
INSTANCE_SCHEMA=<HarperDB instance schema>
GOOGLE_CLIENT_ID=<Google client ID>
JWT_GOOGLE_SECRET=<JWT secret - can be anything>
CLIENT_DOMAIN=<Client code domain(eg: http://localhost:3000)>
```


Then run the following commands:

1. To install dependencies

```
npm install
```

2. To run the code

```
npm start
```


