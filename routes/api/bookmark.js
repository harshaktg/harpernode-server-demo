const express = require("express");
const db = require("../../db/config");
const { addCreatedUser } = require("../../utils/postUserUtil");
const { isAuthorised } = require("../../utils/isAuthorized");

const router = express.Router();

/**
 * Get all bookmarks
 */
router.get("/", isAuthorised, async (req, res) => {
  const queryBookmarks = `select bookmarks from ${process.env.INSTANCE_SCHEMA}.user where user_id='${req.user.user_id}'`;
  const queryAllPosts = `select * from ${process.env.INSTANCE_SCHEMA}.post order by __createdtime__ DESC`;

  try {
    const bookmarksQuery = await db.query(queryBookmarks);
    const allPostsQuery = await db.query(queryAllPosts);
    const allBookmarks = bookmarksQuery.data?.[0]?.bookmarks;
    const allPosts = allPostsQuery.data;
    console.log("FOLLOWING USERS ARE", allBookmarks);
    console.log("allPosts ARE", allPosts);
    if (allBookmarks.length) {
      const processedPosts = allPosts.filter((pst) =>
        allBookmarks.includes(pst.post_id)
      );
      const finalPosts = await addCreatedUser(processedPosts);
      res.status(200).json({
        success: true,
        data: finalPosts,
      });
    } else {
      const finalPosts = await addCreatedUser(allBookmarks);
      res.status(200).json({
        success: true,
        data: finalPosts,
      });
    }
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Add Bookmark
 */
router.post("/add", isAuthorised, async (req, res) => {
  const { postId } = req.body;
  if (!postId) {
    return res.status(500).json({ error: "Internal server error" });
  }

  // if (req.user.bookmarks.includes(postId)) {
  //   return res.status(503).json({ error: "Already bookmarked" });
  // }

  const updatedBookmarks = [...req.user.bookmarks, postId];

  const options = {
    table: "user",
    records: [
      {
        bookmarks: updatedBookmarks,
        user_id: req.user.user_id,
      },
    ],
  };

  try {
    const resp = await db.update(options);
    res.status(200).json({
      success: true,
      data: "Bookmarked successfully",
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Remove Bookmark
 */
router.post("/remove", isAuthorised, async (req, res) => {
  const { postId } = req.body;
  if (!postId) {
    return res.status(500).json({ error: "Internal server error" });
  }

  // if (!req.user.bookmarks.includes(postId)) {
  //   return res.status(503).json({ error: "Not bookmarked already" });
  // }

  const updatedBookmarks = req.user.bookmarks.filter(
    (pstId) => pstId !== postId
  );

  const options = {
    table: "user",
    records: [
      {
        bookmarks: updatedBookmarks,
        user_id: req.user.user_id,
      },
    ],
  };

  try {
    const resp = await db.update(options);
    res.status(200).json({
      success: true,
      data: "Removed bookmark successfully",
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
