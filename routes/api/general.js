const express = require("express");
const app = express();
const router = express.Router();
const { isAuthorised } = require("../../utils/isAuthorized");
const db = require("../../db/config");
const { handleResponse } = require("../../utils/handleResponse");
const { addCreatedUser } = require("../../utils/postUserUtil");

router.get("/currentUser", isAuthorised, async (req, res) => {
  console.log("req.user", req.user);
  return res.status(200).json(req.user);
});

router.get("/feed", isAuthorised, async (req, res) => {
  const followingUsers = req.user.following;
  const followingTags = req.user.tags;

  const queryAllPosts = `select * from ${process.env.INSTANCE_SCHEMA}.post where created_user!='${req.user.user_id}' order by __createdtime__ DESC`;

  try {
    const allPostsQuery = await db.query(queryAllPosts);
    const allPosts = allPostsQuery.data;
    const processedPosts = allPosts.filter(
      (post) =>
        followingUsers.includes(post.created_user) ||
        followingTags.filter((tag) => post.tags.includes(tag)).length > 0
    );
    const finalPosts = await addCreatedUser(processedPosts);
    const finalAllPosts = await addCreatedUser(allPosts);
    res.status(200).json({
      success: true,
      data: finalPosts.length > 0 ? finalPosts : finalAllPosts,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/column", (request, response) => {
  db.dropAttribute(
    {
      schema: process.env.INSTANCE_SCHEMA,
      table: request.body.table,
      attribute: request.body.attribute,
    },
    (err, res) => {
      handleResponse(err, res, response);
    }
  );
});

module.exports = router;
