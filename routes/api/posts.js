const express = require("express");
const db = require("../../db/config");
const { isAuthorised } = require("../../utils/isAuthorized");
const { addCreatedUser } = require("../../utils/postUserUtil");

const router = express.Router();

router.post("/", isAuthorised, async (req, res) => {
  const options = {
    table: "post",
    records: [
      {
        ...req.body,
        created_user: req.user.user_id,
        tags: req.body.tags || [],
        likes: [],
        comments: [],
      },
    ],
  };

  const searchOptions = {
    table: "post",
    attributes: ["*"],
  };

  try {
    const insertRes = await db.insert(options);
    const post = await db.searchByHash({
      ...searchOptions,
      hashValues: insertRes.data.inserted_hashes,
    });
    const postObj = post.data[0];
    res.status(201).json({ success: true, data: "Successfully Posted!" });
  } catch (err) {
    console.log(err);
    res.status(404).json({ error: err });
  }
});

/**
 * Filter post based on Tags
 */
router.post("/tags", async (req, res) => {
  const { tags } = req.body;
  let query = `select * from ${process.env.INSTANCE_SCHEMA}.post order by __createdtime__ DESC`;

  try {
    const allPostsQuery = await db.query(query);
    const allPosts = allPostsQuery.data;
    const processedPosts = allPosts.filter(
      (post) => tags.filter((tag) => post.tags.includes(tag)).length > 0
    );
    const finalPosts = await addCreatedUser(processedPosts);
    res.status(200).json({
      success: true,
      data: finalPosts,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/tags/:tagId", async (req, res) => {
  let query = `select * from ${process.env.INSTANCE_SCHEMA}.post where tags like '%${req.params.tagId}%'`;

  try {
    const allPostsQuery = await db.query(query);
    const allPosts = allPostsQuery.data;
    const finalPosts = await addCreatedUser(allPosts);
    res.status(200).json({
      success: true,
      data: finalPosts,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/users/:userId", async (req, res) => {
  let query = `select * from ${process.env.INSTANCE_SCHEMA}.post where created_user='${req.params.userId}'`;

  try {
    const allPostsQuery = await db.query(query);
    const allPosts = allPostsQuery.data;
    const finalPosts = await addCreatedUser(allPosts);
    res.status(200).json({
      success: true,
      data: finalPosts,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/like/:postId", isAuthorised, async (req, res) => {
  const { postId } = req.params;
  const query = `select likes from ${process.env.INSTANCE_SCHEMA}.post where post_id='${postId}'`;

  try {
    const allLikesQuery = await db.query(query);
    const allLikes = allLikesQuery.data?.[0]?.likes;
    if (allLikes.includes(req.user.user_id)) {
      res.status(400).json({ error: "Already Liked" });
    } else {
      const options = {
        table: "post",
        records: [
          {
            likes: [...allLikes, req.user.user_id],
            post_id: postId,
          },
        ],
      };
      const resp = await db.update(options);
      res.status(200).json({
        success: true,
        data: "Liked successfully",
      });
    }
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/unlike/:postId", isAuthorised, async (req, res) => {
  const { postId } = req.params;
  const query = `select likes from ${process.env.INSTANCE_SCHEMA}.post where post_id='${postId}'`;

  try {
    const allLikesQuery = await db.query(query);
    const allLikes = allLikesQuery.data?.[0]?.likes;
    if (!allLikes.includes(req.user.user_id)) {
      res.status(400).json({ error: "Never Liked To Unlike" });
    } else {
      const options = {
        table: "post",
        records: [
          {
            likes: allLikes.filter((like) => like !== req.user.user_id),
            post_id: postId,
          },
        ],
      };
      const resp = await db.update(options);
      res.status(200).json({
        success: true,
        data: "Unliked successfully",
      });
    }
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/:postId", isAuthorised, async (req, res) => {
  const { postId } = req.params;
  const options = {
    table: "post",
    records: [
      {
        ...req.body,
        ...{ post_id: postId },
      },
    ],
  };

  try {
    const resp = await db.update(options);
    res.status(200).json({
      success: true,
      data: "Updated successfully",
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/:postId", isAuthorised, async (req, res) => {
  const { postId } = req.params;
  const options = {
    table: "post",
    hashValues: [postId],
  };

  try {
    const resp = await db.delete(options);
    res.status(200).json({
      success: true,
      data: "Post Deleted successfully",
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/topPosts", async (req, res) => {
  const topPostsQuery = `select * from ${process.env.INSTANCE_SCHEMA}.post ORDER BY __createdtime__ DESC LIMIT 10`;
  try {
    const topPosts = await db.query(topPostsQuery);
    const allPosts = topPosts.data || [];
    // to sort based on likes
    const processedPosts = allPosts?.sort((a, b) => {
      if (a.likes.length > b.likes.length) {
        return -1;
      }
      if (a.likes.length < b.likes.length) {
        return 1;
      }
      return 0;
    });

    const finalPosts = await addCreatedUser(processedPosts);
    res.status(200).json({
      success: true,
      data: finalPosts,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/myPosts", isAuthorised, async (req, res) => {
  const query = `select * from ${process.env.INSTANCE_SCHEMA}.post where created_user='${req.user.user_id}' ORDER BY __createdtime__ DESC`;

  try {
    const allPostsQuery = await db.query(query);
    const allPosts = allPostsQuery.data;

    const finalPosts = await addCreatedUser(allPosts);
    res.status(200).json({
      success: true,
      data: finalPosts,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/", async (req, res) => {
  const query = `select * from ${process.env.INSTANCE_SCHEMA}.post ORDER BY __createdtime__ DESC`;

  try {
    const allPostsQuery = await db.query(query);
    const allPosts = allPostsQuery.data;
    const finalPosts = await addCreatedUser(allPosts);
    res.status(200).json({
      success: true,
      data: finalPosts,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Route to get current post
 */
router.get("/:postId", async (req, res) => {
  const { postId } = req.params;
  const query = `select * from ${process.env.INSTANCE_SCHEMA}.post where post_id='${postId}'`;

  try {
    const allPostsQuery = await db.query(query);
    const allPosts = allPostsQuery.data;
    const finalPosts = await addCreatedUser(allPosts);
    if (allPosts.length) {
      res.status(200).json({
        success: true,
        data: finalPosts[0],
      });
    } else {
      res.status(400).json({ error: "No such post" });
    }
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
