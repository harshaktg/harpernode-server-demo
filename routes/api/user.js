const express = require("express");
const app = express();
const jwt = require("jsonwebtoken");
const router = express.Router();
const { config } = require("dotenv");
config();
const db = require("../../db/config");
const { isAuthorised } = require("../../utils/isAuthorized");

/**
 * Get top users that has max followers
 */

router.get("/topUsers", async (req, res) => {
  const topFollowerQuery = `SELECT user_id, COUNT(*) AS cnt FROM ${process.env.INSTANCE_SCHEMA}.user_followers GROUP BY user_id ORDER BY cnt DESC LIMIT 10`;
  try {
    const topUsers = await db.query(topFollowerQuery);
    const userIds = topUsers.data.map((user) => user.user_id);
    const followerOptions = {
      table: "user",
      hashValues: userIds,
      attributes: ["*"],
    };

    const allUsers = await db.searchByHash(followerOptions);
    res.status(200).json({
      success: true,
      data: allUsers.data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Get user based on ID
 */
router.get("/:userId", async (req, res) => {
  const { userId } = req.params;
  const getUser = `select * from ${process.env.INSTANCE_SCHEMA}.user where user_id='${userId}'`;
  const followersQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE user_id='${userId}'`;
  const followingQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE follower_id='${userId}'`;

  try {
    const allUsersQuery = await db.query(getUser);
    const allFollowers = await db.query(followersQuery);
    const allFollowing = await db.query(followingQuery);

    const followersIds = allFollowers.data.map(
      (follower) => follower.follower_id
    );
    const followingIds = allFollowing.data.map((follower) => follower.user_id);

    const allUsers = allUsersQuery.data;
    if (allUsers.length) {
      res.status(200).json({
        success: true,
        data: {
          ...allUsers[0],
          followers: followersIds,
          following: followingIds,
        },
      });
    } else {
      res.status(404).json({ error: "No such user" });
    }
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/all", async (req, res) => {
  const queryAllUsers = `select * from ${process.env.INSTANCE_SCHEMA}.user`;

  try {
    const allUsers = await db.query(queryAllUsers);
    res.status(200).json({
      success: true,
      data: allUsers.data,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Get following users for a user
 */
router.get("/following/:userId", async (req, res) => {
  const { userId } = req.params;
  const followingQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE follower_id='${userId}'`;

  try {
    const allFollowingsRecords = await db.query(followingQuery);

    const followingIds = allFollowingsRecords.data.map(
      (following) => following.user_id
    );

    if (followingIds.length === 0) {
      return res.status(200).json({
        success: true,
        data: [],
      });
    }

    const followingOptions = {
      table: "user",
      hashValues: followingIds,
      attributes: ["*"],
    };

    const allUsers = await db.searchByHash(followingOptions);
    res.status(200).json({
      success: true,
      data: allUsers.data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Get followers for a user
 */
router.get("/followers/:userId", async (req, res) => {
  const { userId } = req.params;
  const queryfollowers = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE user_id='${userId}'`;

  try {
    const allFollowersRecords = await db.query(queryfollowers);

    const followersIds = allFollowersRecords.data.map(
      (follower) => follower.follower_id
    );

    if (followersIds.length === 0) {
      return res.status(200).json({
        success: true,
        data: [],
      });
    }

    const followerOptions = {
      table: "user",
      hashValues: followersIds,
      attributes: ["*"],
    };

    const allUsers = await db.searchByHash(followerOptions);
    res.status(200).json({
      success: true,
      data: allUsers.data,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Update user
 */
router.patch("/", isAuthorised, async (req, res) => {
  if (!req.body.name.trim()) {
    return res.status(500).json({ error: "Name can't be blank" });
  }
  const options = {
    table: "user",
    records: [
      {
        ...req.body,
        ...{ user_id: req.user.user_id },
      },
    ],
  };

  try {
    const resp = await db.update(options);
    res.status(200).json({
      success: true,
      data: "Updated successfully",
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Follow user
 */
router.post("/follow", isAuthorised, async (req, res) => {
  const { userId } = req.body;
  if (!userId) {
    return res.status(500).json({ error: "Internal server error" });
  }

  const checkExistingFollowerQuery = `select * from ${process.env.INSTANCE_SCHEMA}.user_followers where user_id='${req.user.user_id}' AND follower_id='${userId}'`;
  try {
    const follower = await db.query(checkExistingFollowerQuery);
    if (follower.length > 0) {
      return res.status(503).json({ error: "User already followed" });
    } else {
      const options = {
        table: "user_followers",
        records: [
          {
            user_id: userId,
            follower_id: req.user.user_id,
          },
        ],
      };

      const resp = await db.insert(options);
      res.status(200).json({
        success: true,
        data: "Followed successfully",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Unfollow user
 */
router.post("/unfollow", isAuthorised, async (req, res) => {
  const { userId } = req.body;
  if (!userId) {
    return res.status(500).json({ error: "Internal server error" });
  }

  const checkExistingFollowerQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE follower_id='${req.user.user_id}' AND user_id='${userId}'`;
  try {
    const follower = await db.query(checkExistingFollowerQuery);
    if (follower.length == 0) {
      return res.status(503).json({ error: "User not followed" });
    } else {
      const deleteFollowerQuery = `DELETE FROM ${process.env.INSTANCE_SCHEMA}.user_followers WHERE follower_id='${req.user.user_id}' AND user_id='${userId}'`;

      const resp = await db.query(deleteFollowerQuery);
      res.status(200).json({
        success: true,
        data: "Unfollowed successfully",
      });
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Delete user
 */
router.delete("/", isAuthorised, async (req, res) => {
  const options = {
    table: "user",
    hashValues: [req.user.user_id],
  };

  try {
    const resp = await db.delete(options);
    res.status(200).json({
      success: true,
      message: "User Deleted successfully",
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
