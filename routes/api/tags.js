const express = require("express");
const db = require("../../db/config");
const { isAuthorised } = require("../../utils/isAuthorized");
const { TAGS } = require("../../utils/tags");

const router = express.Router();

/**
 * Get all tags
 */
router.get("/", async (req, res) => {
  const query = `select * from ${process.env.INSTANCE_SCHEMA}.tag`;

  try {
    const allTagsQuery = await db.query(query);
    const allTags = allTagsQuery.data;
    res.status(200).json({
      success: true,
      data: allTags,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

/**
 * Get particular tag
 * @todo Method not exposed yet
 */
// router.get("/:tagId", async (req, res) => {
//   const { tagId } = req.params;
//   const query = `select * from ${process.env.INSTANCE_SCHEMA}.tag where tag_id='${tagId}'`;

//   try {
//     const allTagsQuery = await db.query(query);
//     const allTags = allTagsQuery.data;
//     if (allTags.length) {
//       res.status(200).json({
//         success: true,
//         data: allTags[0],
//       });
//     } else {
//       res.status(400).json({ error: "No such tag" });
//     }
//   } catch (err) {
//     res.status(500).json({ error: "Internal server error" });
//   }
// });

/**
 * Create a tag
 * @todo Method not exposed yet
 */
// router.post("/", isAuthorised, async (req, res) => {
//   const { name, description } = req.body;
//   const options = {
//     table: "tag",
//     records: [
//       {
//         name,
//         description,
//       },
//     ],
//   };

//   const searchOptions = {
//     table: "tag",
//     attributes: ["*"],
//   };

//   const existingSearchOptions = {
//     table: "tag",
//     searchAttribute: "name",
//     searchValue: name,
//     attributes: ["*"],
//   };

//   const queryAllTags = `select count(*) from ${process.env.INSTANCE_SCHEMA}.tag`;

//   try {
//     const allTags = await db.query(queryAllTags);
//     if (allTags.data?.[0]?.["COUNT(*)"] > 0) {
//       const existingTag = await db.searchByValue(existingSearchOptions);
//       if (existingTag.data.length) {
//         const tagObj = existingTag.data[0];
//         return res.status(201).json({ success: true, data: tagObj });
//       }
//     }
//     const insertRes = await db.upsert(options);
//     const tag = await db.searchByHash({
//       ...searchOptions,
//       hashValues: insertRes.data.upserted_hashes,
//     });
//     const tagObj = tag.data[0];
//     console.log("tag OBJ IS", tagObj);
//     res.status(201).json({ success: true, data: tagObj });
//   } catch (err) {
//     res.status(404).json({ error: err });
//   }
// });

/**
 * Update a tag
 * @todo Method not exposed yet
 */
// router.patch("/:tagId", isAuthorised, async (req, res) => {
//   const { tagId } = req.params;
//   const options = {
//     table: "tag",
//     records: [
//       {
//         ...req.body,
//         ...{ tag_id: tagId },
//       },
//     ],
//   };

//   try {
//     const resp = await db.update(options);
//     res.status(200).json({
//       success: true,
//       message: "Updated successfully",
//     });
//   } catch (err) {
//     res.status(500).json({ error: "Internal server error" });
//   }
// });

/**
 * Delete a tag
 * @todo Method not exposed yet
 */
// router.delete("/delete/:tagId", isAuthorised, async (req, res) => {
//   const { tagId } = req.params;
//   const options = {
//     table: "tag",
//     hashValues: [tagId],
//   };

//   try {
//     const resp = await db.delete(options);
//     res.status(200).json({
//       success: true,
//       message: "Tag Deleted successfully",
//     });
//   } catch (err) {
//     console.log(err);
//     res.status(500).json({ error: "Internal server error" });
//   }
// });

/**
 * Bulk edit tags from HashNode
 * @todo Method not exposed yet
 */
// router.post("/addBulk", isAuthorised, async (req, res) => {
//   const options = {
//     table: "tag",
//     records: TAGS,
//   };

//   try {
//     const resp = await db.insert(options);
//     res.status(201).json({ success: true, data: "Inserted tags successfully" });
//   } catch (err) {
//     res.status(404).json({ error: err });
//   }
// });

module.exports = router;
