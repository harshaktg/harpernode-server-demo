const express = require("express");
const db = require("../../db/config");
const { isAuthorised } = require("../../utils/isAuthorized");
const { addCreatedUser } = require("../../utils/postUserUtil");

const router = express.Router();

router.get("/posts", async (req, res) => {
  const { searchKey } = req.query;
  const query = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.post WHERE title LIKE '%${searchKey}%' OR subTitle LIKE '%${searchKey}%'`;

  try {
    const postsQuery = await db.query(query);
    const allPosts = postsQuery.data;
    res.status(200).json({
      success: true,
      data: allPosts,
    });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/users", async (req, res) => {
  const { searchKey } = req.query;
  const query = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user WHERE name LIKE '%${searchKey}%'`;

  try {
    const usersQuery = await db.query(query);
    const allUsers = usersQuery.data;
    res.status(200).json({
      success: true,
      data: allUsers,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/tags", async (req, res) => {
  const { searchKey } = req.query;
  const query = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.tag WHERE name LIKE '%${searchKey}%'`;

  try {
    const tagsQuery = await db.query(query);
    const allTags = tagsQuery.data;
    res.status(200).json({
      success: true,
      data: allTags,
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

router.get("/", async (req, res) => {
  const { searchKey } = req.query;
  const usersQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.user WHERE name LIKE '%${searchKey}%'`;
  const postsQuery = `SELECT * FROM ${process.env.INSTANCE_SCHEMA}.post WHERE title LIKE '%${searchKey}%' OR subTitle LIKE '%${searchKey}%' order by __createdtime__ DESC`;
  try {
    const usersData = await db.query(usersQuery);
    const postsData = await db.query(postsQuery);
    const finalUsers = usersData.data;
    const finalPosts = await addCreatedUser(postsData.data);
    res.status(200).json({
      success: true,
      data: {
        users: finalUsers,
        posts: finalPosts,
      },
    });
  } catch (err) {
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
