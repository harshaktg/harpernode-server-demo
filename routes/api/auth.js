const express = require("express");
const jwt = require("jsonwebtoken");
const app = express();
const { nanoid } = require("nanoid");
const router = express.Router();
const { OAuth2Client } = require("google-auth-library");
const { config } = require("dotenv");
config();
const db = require("../../db/config");
const { isAuthorised } = require("../../utils/isAuthorized");

const client = new OAuth2Client(process.env.CLIENT_ID);

router.post("/google", async (req, res) => {
  const { token } = req.body;
  const ticket = await client.verifyIdToken({
    idToken: token,
    audience: process.env.CLIENT_ID,
  });
  const { sub, name, email, picture } = ticket.getPayload();
  const options = {
    table: "user",
    records: [
      {
        name,
        email,
        picture,
        gauth_id: `gid-${sub}`,
        following: [],
        tags: [],
        bookmarks: [],
      },
    ],
  };

  console.log("GAUTH OPTIONS", options);

  const searchOptions = {
    table: "user",
    attributes: ["*"],
  };

  const existingSearchOptions = {
    table: "user",
    searchAttribute: "gauth_id",
    searchValue: `gid-${sub}`,
    attributes: ["*"],
  };

  const queryAllUsers = `select count(*) from ${process.env.INSTANCE_SCHEMA}.user`;

  try {
    const allUsers = await db.query(queryAllUsers);
    if (allUsers.data?.[0]?.["COUNT(*)"] > 0) {
      const existingUser = await db.searchByValue(existingSearchOptions);
      console.log("EXISTING USER", existingUser);
      if (existingUser.data.length) {
        const userObj = existingUser.data[0];
        console.log("EXISTING USER ", userObj);
        const token = jwt.sign(userObj, process.env.JWT_GOOGLE_SECRET);
        console.log("TOKEN ", token);
        return res.status(201).json({ success: true, token });
      }
    }
    const insertRes = await db.upsert(options);
    const user = await db.searchByHash({
      ...searchOptions,
      hashValues: insertRes.data.upserted_hashes,
    });
    const userObj = user.data[0];
    console.log("USER OBJ IS", userObj);
    const token = jwt.sign(userObj, process.env.JWT_GOOGLE_SECRET);
    console.log("TOKEN OBJ IS", userObj);
    res.status(201).json({ success: true, token });
  } catch (err) {
    res.status(404).json({ error: err });
  }
});

router.delete("/logout", isAuthorised, (req, res) => {
  req.user = null;
  res.status(200).json({
    success: true,
    message: "Logged out successfully",
  });
});

module.exports = router;
